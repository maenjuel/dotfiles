set shiftwidth=2
set tabstop=2
set autoindent
set smartindent
set nu
set laststatus=2

syntax on
colorscheme solarized
set background=dark

execute pathogen#infect()
filetype plugin indent on

let g:lightline = {
	\ 'colorscheme': 'solarized',
	\ }
